/**
 * npm依赖模块:
 * 1. sync-request -->npm install sync-request
 * 2. express --> npm install express
 * @author www.juliangip.com
 */

module.exports = {

    client: require('./API/clients'),
    utils: require('./API/UrlUtils'),
    // server: require('./API/server.js')
}

